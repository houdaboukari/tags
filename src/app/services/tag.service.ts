import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Tag } from '../models/tag';

@Injectable({
  providedIn: 'root'
})
export class TagService {

  //Declaration of string some Tag
  someTag : string = "Some Tag";
  //Declaration of list of objects(Tag) : "TagList"
  tagList: Tag[] = [{id :1, title :"M tag"}, {id :2, title :"M2 tag"}, {id :3, title :"M3 tag"}];
  //Declaration of string masterTag
  masterTag : string = "M Tag";

  constructor(private _snackBar: MatSnackBar) { }


  deleteTag(item : Tag) {
    let index = this.tagList.indexOf(item);
    this.tagList.splice(-1);

    this._snackBar.open("Last tag succefully deleted","ok");
  }

  addTag(title : string) {
    let id = this.tagList.length + 1;

    const item: Tag = {
      id: id,
      title: title
    }

    console.log("tag : ", item)
    this.tagList.push(item);
  }
}
