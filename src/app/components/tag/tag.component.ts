import { Component, Input, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Tag } from 'src/app/models/tag';
import { TagService } from 'src/app/services/tag.service';

@Component({
  selector: 'app-tag',
  templateUrl: './tag.component.html',
  styleUrls: ['./tag.component.css']
})
export class TagComponent implements OnInit {

  //Data sharing between components: list-tag and tag
  @Input() tag!: Tag;

  //Call the service "tagService"
  constructor(public tagService: TagService) { }

  ngOnInit(): void {
  }

}
