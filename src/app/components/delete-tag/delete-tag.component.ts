import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Tag } from 'src/app/models/tag';
import { TagService } from 'src/app/services/tag.service';

@Component({
  selector: 'app-delete-tag',
  templateUrl: './delete-tag.component.html',
  styleUrls: ['./delete-tag.component.css']
})
export class DeleteTagComponent implements OnInit {

  tag!: Tag;

  //Call the service and library needed :  "tagService" and "MatSnackBar"
  constructor(public tagService: TagService, private _snackBar: MatSnackBar) { }

  ngOnInit(): void {
  }

  //delete last tag from list
  //item : is the last tag selected from list-tag
  deleteTag(item : Tag) {
    this.tag = item;
    this.tagService.deleteTag(item);
    //Display a message in the snackbar when the deletion is done
    this._snackBar.open("Tag succefully deleted","ok",{
      duration: 3000
    });
    console.log("tag deleted !");
  }
}
