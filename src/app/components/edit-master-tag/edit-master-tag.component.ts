import { Component, Input, OnInit } from '@angular/core';
import { TagService } from 'src/app/services/tag.service';

@Component({
  selector: 'app-edit-master-tag',
  templateUrl: './edit-master-tag.component.html',
  styleUrls: ['./edit-master-tag.component.css']
})
export class EditMasterTagComponent implements OnInit {
  //Data sharing between components: list-tag and edit-master-tag
  @Input() MasterTagInput!: string;
  //Call the service "tagService"
  constructor(public tagService : TagService) { }
  ngOnInit(): void {
  }

}
