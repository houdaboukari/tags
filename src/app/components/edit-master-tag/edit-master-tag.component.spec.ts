import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditMasterTagComponent } from './edit-master-tag.component';

describe('EditMasterTagComponent', () => {
  let component: EditMasterTagComponent;
  let fixture: ComponentFixture<EditMasterTagComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditMasterTagComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditMasterTagComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
