import { Component, Input, OnInit } from '@angular/core';
import { TagService } from 'src/app/services/tag.service';

@Component({
  selector: 'app-edit-tag',
  templateUrl: './edit-tag.component.html',
  styleUrls: ['./edit-tag.component.css']
})
export class EditTagComponent implements OnInit {

  //Data sharing between components: list-tag and edit-tag
  @Input() SomeTagInput!: string;
  //Call the service "tagService"
  constructor(public tagService : TagService) { }

  ngOnInit(): void {
  }

}
