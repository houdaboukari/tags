import { Component, OnInit } from '@angular/core';
import { TagService } from 'src/app/services/tag.service';

@Component({
  selector: 'app-list-tags',
  templateUrl: './list-tags.component.html',
  styleUrls: ['./list-tags.component.css']
})
export class ListTagsComponent implements OnInit {

  //Call the service "tagService"
  constructor(public tagService: TagService) { }

  //declare limit of items to display
  limit = 3;
  ngOnInit(): void {
  }

  //trackedBy "id" : used to track our incoming data every time we receive a request from an API
  trackByTag(index : number, tag : any) : number{
    return tag.id;
  }

}
