import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { TagService } from 'src/app/services/tag.service';

@Component({
  selector: 'app-add-tag',
  templateUrl: './add-tag.component.html',
  styleUrls: ['./add-tag.component.css']
})
export class AddTagComponent implements OnInit {

  //declaration of string variable "tag" as empty string
  tag = '';

  // call library needs and services
  constructor(public tagService: TagService,
    private _snackBar: MatSnackBar) { }

  ngOnInit(): void {
  }

  //Add tag method
  onSubmit(){
    //Call service addTag
    this.tagService.addTag(this.tag);

    this.tag = ''; //this line is to empty the input

    //Display a message in the snackbar when the add tag is done
    this._snackBar.open("Tag succefully added","ok",{
      duration: 3000
    });
    console.log("tag Added !");

  }
}
